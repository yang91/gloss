import requests
import base64
import tqdm
import json

def get_lessons(url, page, language_id=""):
    r = requests.post(url, data={
        "action": "search",
        "page": page,
        "searchString": "",
        "languageIds": language_id,
        "levelIds": "",
        "modalityIds": "",
        "competenceIds": "",
        "topicIds": "",
        "subTopicIds": "",
        "video": 0,
        "statusIds": "",
        "mine": 0,
        "sortBy": "alphabetical"
    })
    data = r.json()
    return data


def get_media_files(url):
    r = requests.get(url)
    return r


def main(args):
    url = "https://gloss.dliflc.edu/LessonJSON.aspx"
    language_id = args.lang_id
    page = 0
    page_count = 1
    os.makedirs("data", exist_ok=True)
    while page < page_count:
        page += 1
        data = get_lessons(url, page, language_id)
        page_count = data["pageCount"]
        lessons = data["lessons"]
        for lesson in tqdm.tqdm(lessons):
            lesson_id = lesson["id"]
            download_url = f"https://gloss.dliflc.edu/LessonCounter.aspx?lessonId={lesson_id}&linkTypeId=1"
            if lesson["mediaLink"]:
                r = requests.get(download_url)
                lesson["b64_content"] = base64.b64encode(r.content).decode()
            
        filename = f"data/gloss-data-{page}.json"
        with open(filename, "w") as f:
            json.dump(data, f)

        print(page, page_count)
    

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--lang-id",
        default="",
        help="Language id"
    )
    args, _ = parser.parse_known_args()
    main(args)
